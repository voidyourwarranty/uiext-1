# UIkit 3.0 Tweaks Part 1

This is a small modification to the `uk-accordion` widget in [UIkit 3.0](https://getuikit.com/). The standard UIkit 3.0
framework defines a single animation for the `uk-accordion`. For those cases in which a single accordion element
contains a number of UIkit 3.0 widgets, e.g. `uk-card`, and in which the `uk-scrollspy` can trigger a sequence of
individual animations, we modify the `uk-accordion` accordingly.

The present project was developed with UIkit 3.0.0-rc14.

## Usage

In order to use the modified `uk-accordion` widget, include the javascript file `uiext-1.js` and call `onLoad=UIext1 ()`
in your `<body>` tag.

The original animation of your `uk-accordion` must be switched off, i.e.  
``` 
<ul uk-accordion="animation:false"> 
  [...]
</ul> 
``` 
and each `<li>` element that contains elements with the class `uk-accordion-content`, needs the new class
attribute `class="ux-hide-all-cards"` (yes, it is `ux` for UIkit Extensions). Then the accordion may contain several
`uk-card` in a `uk-grid`, and a `uk-scrollspy` can be used in order to trigger a series of animations. Please refer to
the example file [`uiext-1.html`](https://gitlab.com/voidyourwarranty/uiext-1/blob/master/uiext-1.html)

## License

[MIT License](https://opensource.org/licenses/MIT)
