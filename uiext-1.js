/* -*- mode:javascript -*-
// ================================================================================
// GitLab:    http://gitlab.com/voidyourwarranty/uiext-1
// File:      uiext-1.js
// Date:      2018-09-10
// Author:    voidyourwarranty
// Purpose:   Allow a sequence of animations in the uk-cards within a uk-accordion
// ================================================================================
*/

/*
// In order to use this UIkit 3.0 extension, add the line <script src="uiext-1.js"></script> to the <head> of the <html>
// document, and add the attribute onload="UIext1 ()" to the <body> element.
*/

UIext1 = function () {

    /*
    // --------------------------------------------------------------------------------
    // Modification of the "uk-accordion" animation.
    // --------------------------------------------------------------------------------
    //
    // If the "uk-accordion-content" contains a container of several "uk-card" elements, the standard animation of the
    // "uk-accordion" can be replaced by a sequential animation applied to each "uk-card" that is triggered by a
    // "uk-scrollspy".
    //
    // First, switch off the standard animation by using <ul uk-accordion="animation:false">.
    //
    // Second, each <li> element that contains "uk-accordion-content" needs the new attribute <li
    // class="ux-hide-all-cards">. This makes sure that once the accordion content is closed, the visibility of all
    // cards contained in there is set to "hidden". As a consequence, once the accordion content is opened again, the
    // cards appear for the first time, and the scrollspy can take care of the animations.
    //
    // For example, the "uk-card" elements can be put in a "uk-grid" container as follows:
    // <div class="uk-grid-small uk-text-center uk-grid-match" uk-grid uk-scrollspy="target: > div; cls:uk-animation-fade; delay: 500; repeat:true">
    */

    UIkit.util.on (".ux-hide-all-cards",'hide',function (arg) {
	var cards = this.getElementsByClassName ("uk-card");
	for (var i=0;i<cards.length;i++) {
	    cards[i].style.visibility = "hidden";
	}
    })
}

/*
// ================================================================================
// End of file.
// ================================================================================
*/
